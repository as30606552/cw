import sys
import time

import numpy as np
from PyQt5.QtCore import Qt, QThread, pyqtSignal, QObject
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QTabWidget, QPushButton, QFileDialog, \
    QGridLayout, QLabel, QProgressBar, QLineEdit, QMessageBox
from pyqtgraph import PlotWidget, mkPen, setConfigOptions
from pyqtgraph.exporters import ImageExporter


class Window(QWidget):
    def center(self):
        screen_geometry = QApplication.desktop().frameGeometry()
        window_geometry = self.frameGeometry()
        self.move((screen_geometry.width() - window_geometry.width()) // 2,
                  (screen_geometry.height() - window_geometry.height()) // 2)


class Worker(QThread):
    result_signal = pyqtSignal(tuple)

    def __init__(self):
        super().__init__()
        self.time = 0
        self.stop = False
        self.plt = None
        self.k = 0
        self.iterations = 0

    def plotter(self, plt):
        self.plt = plt

    def value(self, param):
        return self.plt.value(param)

    def interrupt(self, b):
        self.stop = b

    def result(self, x, y):
        self.result_signal.emit((x, y))

    def onResult(self, func):
        self.result_signal.connect(func)

    def validate(self):
        raise Exception('Implement me')


class Checker(QThread):
    progress_signal = pyqtSignal(tuple)
    time_signal = pyqtSignal(int)

    def __init__(self, worker):
        super().__init__()
        self.worker = worker
        self.worker.started.connect(lambda: self.start())

    def run(self):
        ts = time.time()
        iterations = 0
        i = 0
        while self.worker.isRunning():
            for i in range(5):
                QThread.msleep(200)
                iterations = self.worker.iterations
                i = self.worker.k
                self.progress_signal.emit((i, iterations))
            self.time_signal.emit(int((time.time() - ts) * (iterations - i) / i))
        self.progress_signal.emit((self.worker.iterations, self.worker.iterations))
        self.time_signal.emit(0)

    def onProgress(self, func):
        self.progress_signal.connect(func)

    def onTime(self, func):
        self.time_signal.connect(func)


class Label(QLabel):
    def __init__(self, parent, label_str=''):
        super().__init__(label_str, parent)
        font = self.font()
        font.setPointSize(13)
        self.setFont(font)
        self.setFixedHeight(40)

    def italic(self):
        font = self.font()
        font.setItalic(True)
        self.setFont(font)


class Input(QLineEdit):
    def __init__(self, parent, minimum, maximum, value):
        super().__init__(parent)
        self.t = type(value)
        self.minimum = minimum
        self.maximum = maximum
        self.setFixedHeight(40)
        self.setText(str(value))
        font = self.font()
        font.setPointSize(12)
        self.setFont(font)

    def value(self):
        if self.t == int:
            return int(self.text())
        elif self.t == float:
            return float(self.text())
        else:
            return self.text()

    def validate(self):
        try:
            self.text()
            if self.t == int:
                value = int(self.text())
            elif self.t == float:
                value = float(self.text())
            else:
                return True
            return self.minimum <= value <= self.maximum
        except ValueError:
            return False


class Info(QLabel):
    def __init__(self, parent, description):
        super().__init__(parent)
        self.setPixmap(QPixmap('info.png'))
        self.setToolTip(description)


class Button(QPushButton):
    def __init__(self, parent, title, width=100, height=40):
        super().__init__(title, parent)
        if width > 0:
            self.setFixedWidth(width)
        if height > 0:
            self.setFixedHeight(height)

    def onClick(self, func):
        self.clicked.connect(func)


class ProgressSignal(QObject):
    interrupt_signal = pyqtSignal()


class Progress(Window):
    def __init__(self):
        super().__init__()
        self.signal = ProgressSignal()
        layout = QVBoxLayout(self)
        self.act = Label(self)
        layout.addWidget(self.act)
        self.info = Label(self)
        layout.addWidget(self.info)
        self.ts = Label(self)
        layout.addWidget(self.ts)
        layout.addStretch(1)
        self.progress_bar = QProgressBar(self)
        layout.addWidget(self.progress_bar)
        self.setWindowTitle('Вычисление')
        self.setWindowModality(Qt.ApplicationModal)
        self.resize(600, 200)
        self.center()

    def action(self, action):
        self.act.setText(action)

    def progress(self, i, count):
        self.info.setText(f'Выполнено итераций: {i}/{count}')
        self.progress_bar.setValue(int(i * 100 / count))

    def time(self, ts):
        hours = ts // 3600
        minutes = (ts - hours * 3600) // 60
        seconds = (ts - hours * 3600 - minutes * 60)
        self.ts.setText(f'Осталось времени: {hours:0>2}:{minutes:0>2}:{seconds:0>2}')

    def closeEvent(self, event):
        self.signal.interrupt_signal.emit()

    def onInterrupt(self, func):
        self.signal.interrupt_signal.connect(func)


class Plotter(QWidget):
    def __init__(self, parent, params, worker, pen, legend):
        super().__init__(parent)
        self.pen = pen
        self.name = legend
        self.plot = parent.plot
        self.worker = worker
        worker.plotter(self)
        worker.onResult(self.plotting)
        self.progress = Progress()
        self.progress.onInterrupt(lambda: worker.interrupt(True))
        checker = Checker(worker)
        checker.onProgress(lambda t: self.progress.progress(t[0], t[1]))
        checker.onTime(self.progress.time)
        self.params = {}
        layout = QGridLayout(self)
        i = 0
        for param, name, default, description, min_val, max_val in params:
            label = Label(self, name)
            label.italic()
            layout.addWidget(label, i, 0, 1, 3)
            field = Input(self, min_val, max_val, default)
            layout.addWidget(field, i, 3, 1, 6)
            self.params[param] = field
            description += f'\nМинимальное значение: {min_val:f}\nМаксимальное значение: {max_val:f}'
            layout.addWidget(Info(self, description), i, 9, alignment=Qt.AlignCenter)
            i += 1
        btn = Button(self, 'ПОСТРОИТЬ', -1, 60)
        font = btn.font()
        font.setPointSize(16)
        btn.setFont(font)
        btn.onClick(self.start)
        layout.addWidget(btn, i, 0, 1, 8, Qt.AlignBottom)
        btn = Button(self, 'Стереть', -1, 60)
        btn.onClick(lambda: parent.remove(legend))
        layout.addWidget(btn, i, 8, 1, 2, Qt.AlignBottom)

    def value(self, param):
        return self.params[param].value()

    def start(self):
        for param in self.params:
            if not self.params[param].validate():
                QMessageBox.warning(self, 'Ошибка', 'Неверно заданы параметры.\nИсправьте и попробуйте еще раз.')
                return
        if not self.worker.validate():
            QMessageBox.warning(self, 'Ошибка', 'Не выполняется условие устойчивости.\nИсправьте параметры и '
                                                'попробуйте еще раз.')
            return
        self.worker.interrupt(False)
        self.progress.action('Подождите, идет вычисление решения...')
        self.progress.ts.setText('Оценка времени...')
        self.progress.info.setText('Выполнено итераций: 0/?')
        self.progress.progress_bar.setValue(0)
        self.progress.show()
        self.worker.start(QThread.TimeCriticalPriority)

    def plotting(self, t):
        self.progress.action('Построение графика...')
        self.plot(t[0], t[1], self.pen, self.name)
        self.progress.action(f'Готово. Время работы: {self.worker.time} секунд')


class AnalyticalSolution(Worker):
    def run(self):
        n = self.value('count')
        lmd = self.value('lambda')
        e = self.value('epsilon')
        l = self.value('l')
        t = self.value('t') * 10 ** -15
        c = 3 * 10 ** 14
        w = 2 * np.pi * c / lmd
        self.iterations = int((4 * l) / (e * np.pi * lmd))
        u = 0
        z = np.linspace(0.0, l, n)
        ts = time.time()
        for self.k in range(self.iterations):
            if self.stop:
                return
            wn = np.pi * c * (1 + 2 * self.k) / (2 * l)
            denominator = 1 - (wn / w) ** 2
            part = (np.sin(wn * t) - (w / wn) * np.sin(w * t)) / denominator
            u += part * np.sin(wn * z / c)
        u *= lmd / (np.pi * l)
        u += np.sin(w * t)
        self.time = time.time() - ts
        self.result(z, u)

    def validate(self):
        return True


class NumericalSolution(Worker):
    def run(self):
        nz = self.value('I') + 1
        nt = self.value('K') + 1
        lmd = self.value('lambda')
        l = self.value('l')
        t = self.value('t') * 10 ** -15
        c = 3 * 10 ** 14
        w = 2 * np.pi * c / lmd
        hz = l / (nz - 1)
        ht = t / (nt - 1)
        const = (c * ht / hz) ** 2
        z = np.linspace(0.0, l, nz)
        self.iterations = nt
        ts = time.time()
        u = np.zeros(nz, dtype=float)
        u1 = np.zeros(nz, dtype=float)
        u2 = np.zeros(nz, dtype=float)
        for self.k in range(2, nt):
            if self.stop:
                return
            u = np.empty(nz, dtype=float)
            u[0] = np.sin(w * self.k * ht)
            for i in range(1, nz - 1):
                u[i] = const * (u1[i + 1] - 2 * u1[i] + u1[i - 1]) + 2 * u1[i] - u2[i]
            u[-1] = const * 2 * (u1[-2] - u1[-1]) + 2 * u1[-1] - u2[-1]
            u1, u2 = u, u1
        self.time = time.time() - ts
        self.result(z, u)

    def validate(self):
        nz = self.value('I') + 1
        nt = self.value('K') + 1
        l = self.value('l')
        t = self.value('t') * 10 ** -15
        c = 3 * 10 ** 14
        return c * t * nz <= l * nt


class NumericalSolutionSmallPrecise(Worker):
    def run(self):
        nz = self.value('I') + 1
        nt = self.value('K') + 1
        lmd = self.value('lambda')
        l = self.value('l')
        t = self.value('t') * 10 ** -15
        c = 3 * 10 ** 14
        w = 2 * np.pi * c / lmd
        hz = l / (nz - 1)
        ht = t / (nt - 1)
        const = (c * ht / hz) ** 2
        z = np.linspace(0.0, l, nz)
        self.iterations = nt
        ts = time.time()
        u = np.zeros(nz, dtype=float)
        u1 = np.zeros(nz, dtype=float)
        u2 = np.zeros(nz, dtype=float)
        for self.k in range(2, nt):
            if self.stop:
                return
            u = np.empty(nz, dtype=float)
            u[0] = np.sin(w * self.k * ht)
            for i in range(1, nz - 1):
                u[i] = const * (u1[i + 1] - 2 * u1[i] + u1[i - 1]) + 2 * u1[i] - u2[i]
            u[-1] = u[-2]
            u1, u2 = u, u1
        self.time = time.time() - ts
        self.result(z, u)

    def validate(self):
        nz = self.value('I') + 1
        nt = self.value('K') + 1
        l = self.value('l')
        t = self.value('t') * 10 ** -15
        c = 3 * 10 ** 14
        return c * t * nz <= l * nt


class MainWindow(Window):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.plot_widget = None
        self.plots = {}
        self.resize(1000, 600)
        self.center()
        self.setWindowTitle('Моделирование электромагнитной волны')
        self.initLayout()

    def initLayout(self):
        main_layout = QHBoxLayout(self)
        main_layout.addLayout(self.initPlotLayout(), 1)
        main_layout.addWidget(self.initParamsLayout())

    def initPlotLayout(self):
        layout = QVBoxLayout()
        self.plot_widget = PlotWidget(self, 'w', enableMenu=False)
        self.plot_widget.showGrid(x=True, y=True)
        self.plot_widget.addLegend()
        self.plot_widget.setLabel('left', '<h3><i>Eₓ</i></h3>')
        self.plot_widget.setLabel('bottom', '<h3><i>z, мкм</i></h3>')
        layout.addWidget(self.plot_widget)
        btn_layout = QHBoxLayout()
        save_btn = Button(self, 'Сохранить')
        save_btn.onClick(self.save)
        btn_layout.addWidget(save_btn)
        clear_btn = Button(self, 'Очистить')
        clear_btn.onClick(self.clear)
        btn_layout.addWidget(clear_btn)
        btn_layout.addStretch(1)
        layout.addLayout(btn_layout)
        return layout

    def initParamsLayout(self):
        tab_sheet = QTabWidget(self)
        tab_sheet.addTab(self.initAnalyticalSolution(), 'Аналитическое решение')
        tab_sheet.addTab(self.initNumericalSolution(), 'Численное решение - Самойлов')
        tab_sheet.addTab(self.initNumericalSolutionSmallPrecise(), 'Численное решение - Головин')
        tab_sheet.setFixedWidth(400)
        return tab_sheet

    def initAnalyticalSolution(self):
        params = [
            ('l', 'L, мкм', 10.0, 'Расстояние до стенки в микрометрах', 1, 10 ** 6),
            ('lambda', 'λ, мкм', 1.0, 'Длина волны в микрометрах', 1, 10 ** 6),
            ('epsilon', 'ε', 0.0001, 'Точность вычислений', 10 ** -8, 0.1),
            ('t', 't, 10⁻¹⁵ с', 3.0, 'Время распространения волны', 10 ** -2, 10 ** 8),
            ('count', 'N', 10 ** 4, 'Количество точек разбиения', 1, 10 ** 8)
        ]
        pen = mkPen(color=(0, 0, 255), cosmetic=True, style=Qt.SolidLine)
        return Plotter(self, params, AnalyticalSolution(), pen, 'аналитическое решение')

    def initNumericalSolution(self):
        params = [
            ('l', 'L, мкм', 10.0, 'Расстояние до стенки в микрометрах', 1, 10 ** 6),
            ('lambda', 'λ, мкм', 1.0, 'Длина волны в микрометрах', 1, 10 ** 6),
            ('t', 't, 10⁻¹⁵ с', 3.0, 'Время распространения волны', 10 ** -2, 10 ** 8),
            ('I', 'I', 10 ** 4, 'Количество интервалов разбиения оси z', 1, 10 ** 8),
            ('K', 'K', 10 ** 4, 'Количество интервалов разбиения оси t', 1, 10 ** 8)
        ]
        pen = mkPen(color=(0, 128, 0), cosmetic=True, style=Qt.SolidLine)
        return Plotter(self, params, NumericalSolution(), pen, 'численное решение - Самойлов')

    def initNumericalSolutionSmallPrecise(self):
        params = [
            ('l', 'L, мкм', 10.0, 'Расстояние до стенки в микрометрах', 1, 10 ** 6),
            ('lambda', 'λ, мкм', 1.0, 'Длина волны в микрометрах', 1, 10 ** 6),
            ('t', 't, 10⁻¹⁵ с', 3.0, 'Время распространения волны', 10 ** -2, 10 ** 8),
            ('I', 'I', 10 ** 4, 'Количество интервалов разбиения оси z', 1, 10 ** 8),
            ('K', 'K', 10 ** 4, 'Количество интервалов разбиения оси t', 1, 10 ** 8)
        ]
        pen = mkPen(color=(255, 0, 0), cosmetic=True, style=Qt.SolidLine)
        return Plotter(self, params, NumericalSolutionSmallPrecise(), pen, 'численное решение - Головин')

    def save(self):
        filename, _ = QFileDialog.getSaveFileName(self, 'Сохранить графики', filter='*.png', directory='../pictures',
                                                  options=QFileDialog.DontUseNativeDialog)
        if filename:
            if not filename.endswith('.png'):
                filename += '.png'
            exporter = ImageExporter(self.plot_widget.getPlotItem())
            exporter.export(filename)

    def clear(self):
        self.plot_widget.clear()

    def remove(self, name):
        if name in self.plots.keys():
            self.plot_widget.removeItem(self.plots[name])
            self.plots.pop(name)

    def plot(self, x, y, pen, name):
        if name in self.plots.keys():
            self.plot_widget.removeItem(self.plots[name])
        setConfigOptions(antialias=True)
        self.plots[name] = self.plot_widget.plot(x, y, pen=pen, name=name)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
