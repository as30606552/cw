import numpy as np


def analytical_solution(c, lmd, z, t, e):
    L = z[-1]
    w = 2 * np.pi * c / lmd
    count = int((4 * L) / (e * np.pi * lmd))
    nz = len(z)
    nt = len(t)
    result = np.empty((nt, nz))
    for k, cur_t in enumerate(t):
        u = np.empty(nz)
        for i in range(count):
            wn = np.pi * c * (1 + 2 * i) / (2 * L)
            denominator = 1 - (wn / w) ** 2
            part = (np.sin(wn * cur_t) - (w / wn) * np.sin(w * cur_t)) / denominator
            u += part * np.sin(wn * z / c)
        u *= lmd / (np.pi * L)
        u += np.sin(w * cur_t)
        result[k] = u
    return result


def numerical_solution(c, lmd, z, t):
    L = z[-1]
    T = t[-1]
    nz = len(z)
    nt = len(t)
    w = 2 * np.pi * c / lmd
    hz = L / (nz - 1)
    ht = T / (nt - 1)
    const = (c * ht / hz) ** 2
    result = np.empty((nt, nz))
    u1 = np.zeros(nz, dtype=float)
    u2 = np.zeros(nz, dtype=float)
    result[0] = u2
    result[1] = u1
    for k in range(2, nt):
        u = np.empty(nz, dtype=float)
        u[0] = np.sin(w * k * ht)
        for i in range(1, nz - 1):
            u[i] = const * (u1[i + 1] - 2 * u1[i] + u1[i - 1]) + 2 * u1[i] - u2[i]
        u[-1] = const * 2 * (u1[-2] - u1[-1]) + 2 * u1[-1] - u2[-1]
        u1, u2 = u, u1
        result[k] = u
    return result


def numerical_solution_small_precise(c, lmd, z, t):
    L = z[-1]
    T = t[-1]
    nz = len(z)
    nt = len(t)
    w = 2 * np.pi * c / lmd
    hz = L / (nz - 1)
    ht = T / (nt - 1)
    const = (c * ht / hz) ** 2
    result = np.empty((nz, nt))
    u1 = np.zeros(nz, dtype=float)
    u2 = np.zeros(nz, dtype=float)
    result[0] = u2
    result[1] = u1
    for k in range(2, nt):
        u = np.empty(nz, dtype=float)
        u[0] = np.sin(w * k * ht)
        for i in range(1, nz - 1):
            u[i] = const * (u1[i + 1] - 2 * u1[i] + u1[i - 1]) + 2 * u1[i] - u2[i]
        u[-1] = u[-2]
        u1, u2 = u, u1
        result[k] = u
    return result


def norm(u):
    return max(np.reshape(u, -1))


def main():
    c = 3.0 * 10 ** 14
    L = 10.0
    T = 3.0 * 10 ** -15
    lmd = 1.0
    e = 0.001
    I = 10
    K = 10
    z = np.linspace(0, L, I * 16)
    t = np.linspace(0, T, K * 16)
    u_a = analytical_solution(c, lmd, z, t, e)
    z = np.linspace(0, L, I)
    t = np.linspace(0, T, K)
    u = numerical_solution(c, lmd, z, t)
    p = 16
    eh = norm(abs(u - u_a[::p, ::p]))
    for n in range(4):
        I *= 2
        K *= 2
        z = np.linspace(0, L, I)
        t = np.linspace(0, T, K)
        u = numerical_solution(c, lmd, z, t)
        p //= 2
        eh_2 = norm(abs(u - u_a[::p, ::p]))
        print(f'I = {I // 2}, K = {K // 2}, e(h) = {eh}, e(h/2) = {eh_2}, delta = {eh / eh_2}')
        eh = eh_2


if __name__ == '__main__':
    main()
